#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      test_units.py
#  brief:     unit tests for units
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Mladen Ivkovic <mladen.ivkovic@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################

import ut_helpers as h
import pNbody.units as u
import string
import pNbody.ctes as cs

def get_unit_list():
    """
    Generates pNbody system of units with random units combinations.
    """   

    u_l = [u.Unit_m, u.Unit_cm, u.Unit_km, u.Unit_Mm, u.Unit_Gm, 
        u.Unit_kpc, u.Unit_Mpc, u.Unit_pc, u.Unit_ua]
    u_t = [u.Unit_s, u.Unit_h, u.Unit_yr, u.Unit_kyr, u.Unit_Gyr, 
        u.Unit_dy, u.Unit_hr, u.Unit_century]
    u_m = [u.Unit_kg, u.Unit_g, u.Unit_Ms, u.Unit_Msol, u.Unit_Mg, 
        u.Unit_Mt, u.Unit_Mj]

    #  So far unused:
    #  u_v = [u.Unit_kmh, u.Unit_kms, u.Unit_ms]
    #
    #  Unit_mol = Units('mol')
    #  Unit_C = Units('C')
    #  Unit_K = Units('K')
    #
    #  Unit_ms2 = Unit_ms**2
    #  Unit_J = Unit_kg * Unit_ms2
    #  Unit_erg = Unit_g * (Unit_cm / Unit_s)**2
    #  Unit_N = Unit_kg * Unit_m / (Unit_s)**2
    #  Unit_Pa = Unit_N / Unit_m**2
    #  Unit_G = Unit_N * Unit_m**2 / Unit_kg**2
    #  Unit_d = Unit_kg / Unit_m**3
    #  Unit_Lsol = 3.839 * Unit_erg / Unit_s

    ulist = [h.r.choice(u_l),
        h.r.choice(u_m), h.r.choice(u_t)]

    return ulist





#  h.announce("UnitSystem")
#  for i in h.repeat:
#      us = h.get_system_of_units()
#      us.info()
#
#  h.announce("UnitSystem.conversion_factor_to")
#  for i in h.repeat:
#      us1 = h.get_system_of_units()
#      ulist = get_unit_list()
#      for un in range(len(ulist)):
#          h.hprint(us1.convertionFactorTo(ulist[un]))
#      print()
#
#  h.announce("UnitSysten.into")
#  for i in h.repeat:
#      us1 = h.get_system_of_units()
#      ulist = get_unit_list()
#      for un in range(len(ulist)):
#          h.hprint(us1.into(ulist[un]))
#      print()
#
#
#
#  h.announce("Units, Units.get_basefactor")
#  for i in h.repeat:
#      ulist = get_unit_list()
#      factor = h.r.randint(0,10)
#      power = h.r.randint(0,10)
#      symbol = h.r.choice(string.ascii_letters)
#      un = u.Units(symbol, factor, power, ulist)
#      print(un, un.get_basefactor())
#

h.announce("PhysCte")
# test for scalars
for i in h.repeat:
    ulist = get_unit_list()
    for un in ulist:
        ct = u.PhysCte(h.r.random(), un)
        h.hprint(ct)
print()

# test for lists
vals = [h.r.random() for i in h.repeat]
un1, un2, un3 = get_unit_list()
print(u.PhysCte(vals, u.Unit_dy))

# Test for arrays
print(u.PhysCte(h.getArr(), un2))

# Test operations
print(u.PhysCte(h.getArr(), un1 )*u.PhysCte(h.getArr(),un2))
print(u.PhysCte(h.getArr(), un1 )/u.PhysCte(3,un2))


h.announce("ctes.convert_ctes()")
for i in h.repeat:
    us = get_unit_list()
    print(us[0], us[1], us[2])
    cs.convert_ctes(us)
    print(cs.BOLTZMANN)
    print()
print()


# TODO:
#  u.Set_SystemUnits_From_File()
#  u.Set_SystemUnits_From_Params()
