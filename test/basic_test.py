#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      basic_test.py
#  brief:     basic test: print info for empty nbody object
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from pNbody import *

nb = Nbody()
nb.info()
