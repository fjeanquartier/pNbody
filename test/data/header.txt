'''
@package   pNbody
@file      \{FILE\}
@brief     \{BRIEF\}
@copyright GPLv3
@author    \{AUTHORS\} <\{EMAIL\}>
@section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL
'''
