#!/bin/bash

###########################################################################################
#  package:   pNbody
#  file:      coverage.sh
#  brief:     Run tests with coverage
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Loic Hausammann <loic_hausammann@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################

rm .coverage
rm .coverage.*


function cleanup {
    # clean up after yourself after you're done playing
    rm *.dat
    rm *.dmp
    rm params*
    rm -r png
    rm img.fits merge.gad dsph.dat output
}

function test {
    # Crash this script if python script crashes
    python3 -m coverage run $1
    CODE=$?
    if [[ $CODE -ne 0 ]]; then
      echo $1 " Exit Code " $CODE
      echo "Test failed"
      cleanup
      exit 1
    fi
}


function mpitest {
    # Crash this script if python script crashes, for scripts using MPI
    if [[ "`mpirun --version`" == *"mpich"* ]]; then
        # assume mpich
        mpirun -n 2 python3 -m coverage run $1
        if [[ $? -ne 0 ]]; then
          echo $1 " Exit Code " $CODE
          echo "Test failed"
          cleanup
          exit 1
        fi
    elif [[ "`mpirun --version`" == *"Open MPI"* ]]; then
        # assume openmpi
        mpirun --allow-run-as-root -n 2 python3 -m coverage run $1
        if [[ $? -ne 0 ]]; then
          echo $1 " Exit Code " $CODE
          echo "Test failed"
          cleanup
          exit 1
        fi
    fi
}




#--------------------------
# Original tests
#--------------------------

test "basic_test.py"
test "io_test.py"
test "format_test.py"
mpitest "pio_test.py"
test "grid_test.py"



#--------------------------
# unit tests
#--------------------------

# test "python3 -m coverage run ut_test_cooling.py" # doesn't work yet
test "ut_test_cosmo.py"
test "ut_test_fourier.py"
test "ut_test_main.py"
test "ut_test_mpi.py"
test "ut_test_thermodyn.py"
test "ut_test_units.py"
mpitest "ut_test_mpi.py"




#--------------------------
# Examples
#--------------------------


mkdir -p png
test "../examples/embedding/test.py"
test "../examples/ic/addmetals/addmetals_for_gas.py data/snap.dat -o output"
# test "../examples/ic/addmetals/addmetals-gasonly.py"    # needs PyChem
# test "../examples/ic/addmetals/addmetals-gas+stars.py"  # needs PyChem
# test "../examples/ic/addmetals/addmetals.py"            # needs PyChem
test "../examples/ic/addmetals/generate_galaxy.py"
test "../examples/ic/generate_cylindrical_model_exponential.py"
test "../examples/ic/generate_cylindrical_model_miyamoto.py"
test "../examples/ic/generate_galaxy.py"
# test "../examples/ic/generate_spherical_model_g2c.py"   # plots stuff
# test "../examples/ic/generate_spherical_model_nfw.py"   # plots stuff
test "../examples/ic/generate_spherical_model_plummer.py"
test "../examples/memory/memory_info.py"
mpitest "../examples/parallel_ic/mkall.py"

cp ../examples/gadget*.dat .
test "../examples/scripts/example01.py"
test "../examples/scripts/example02.py"
test "../examples/scripts/example03.py"
test "../examples/scripts/example04.py"
test "../examples/scripts/example05.py"
# test "../examples/scripts/example06.py"   # plots stuff
# test "../examples/scripts/example07.py"   # plots stuff
test "../examples/scripts/example08.py"
test "../examples/scripts/example10.py"
# test "../examples/scripts/example11.py"   # plots stuff
# test "../examples/scripts/example12.py"   # plots stuff
# test "../examples/scripts/example13.py"   # plots stuff
test "../examples/scripts/slice.py"
test "../examples/scripts/slice-p1.py"
test "../examples/scripts/slice-p2.py"
test "../examples/scripts/makesnapshot.py"
test "../examples/scripts/mkmodel_for_display.py"
# test "../examples/SSP/plot_Lv-Z.py"     # needs Ptools
# test "../examples/SSP/plot_MatLv.py"    # needs Ptools
# test "../examples/SSP/plot_Lv-Age.py"   # needs Ptools
# test "../examples/units/setunits.py"    # hardcoded filename
# test "../examples/units/setunits_2.py"  # hardcoded filename
cp ../examples/units/params* .
cp ../examples/units/dsph.dat .
test "../examples/units/setunits_3.py"
test "../examples/units/setunits_4.py"
test "../examples/units/setunits_5.py"
# test "../examples/units/setunits_6.py"  # has hardcoded file on unige
test "../examples/units/setunits_7.py"
test "../examples/units/setunits_8.py"
test "../examples/units/setunits_9.py"
test "../examples/units/setunits_10.py"


cleanup




python3 -m coverage combine
python3 -m coverage report
python3 -m coverage html
