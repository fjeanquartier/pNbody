#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      test_cosmo.py
#  brief:     unit tests for cosmo.py
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Mladen Ivkovic <mladen.ivkovic@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################

import ut_helpers as h
import pNbody.cosmo as cosmo
from numpy import linspace


atest = linspace(1e-6, 1, 50)
ztest = linspace(0, 1e6, 50)
ttest = linspace(1, 12, 10)


cosmo.setdefault()



h.announce('Z_a')
for a in atest:
    print(cosmo.Z_a(a), end=' ')
print()


h.announce('A_z')
for z in ztest:
    print(cosmo.A_z(z), end=' ')
print()


h.announce('Rho_c')
for i in h.repeat:
    h.hprint(cosmo.Rho_c(h.get_system_of_units()))
print()


h.announce('Hubble_a(a)')
for a in atest:
    h.hprint(cosmo.Hubble_a(a))
print()


h.announce('dt_da(a)')
for i,a in enumerate(atest[:-1]):
    da = atest[i+1] - a
    h.hprint(cosmo.dt_da(da, a))
print()



h.announce('Adot_a(a)')
for a in atest:
    h.hprint(cosmo.Adot_a(a))
print()


h.announce('Age_a(a)')
for a in atest:
    h.hprint(cosmo.Age_a(a))
print()


h.announce('CosmicTime_a(a)')
for a in atest:
    h.hprint(cosmo.CosmicTime_a(a))
print()


h.announce('a_CosmicTime(t)')
for t in ttest:
    h.hprint(cosmo.a_CosmicTime(t))
print()


