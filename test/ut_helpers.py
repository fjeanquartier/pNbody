#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      helpers.py
#  brief:     module containing often used functions for unit tests
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Mladen Ivkovic <mladen.ivkovic@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################


"""
helpers.py

This file contains functions to be used in the
unit test scripts. Mainly commonly used functions
and generating various objects randomly
"""


import random as r


#============================
# Global definitions
#============================

r.seed(1234)                # some random seed
repeat = range(100)         # some list to loop over
cwd=''                      # current workdir



#============================
# Functions
#============================


def announce(name):
    """
    Announce function name to be tested
    """
    
    print("============================================================\n\n")
    print("   Testing ", name)
    print()

    return



def bigrand(expmin=0, expmax=20):
    """
    Generate random number that can have very big value
    expmin: smallest exponent to use
    expmax: biggest exponent to use
    """

    return r.random() * 10.0**r.randint(expmin, expmax)




def get_system_of_units():
    """
    Generates pNbody system of units with random units combinations.
    """   

    from pNbody import units as u
    
    u_l = [u.Unit_m, u.Unit_cm, u.Unit_km, u.Unit_Mm, u.Unit_Gm, 
        u.Unit_kpc, u.Unit_Mpc, u.Unit_pc, u.Unit_ua]
    u_t = [u.Unit_s, u.Unit_h, u.Unit_yr, u.Unit_kyr, u.Unit_Gyr, 
        u.Unit_dy, u.Unit_hr, u.Unit_century]
    u_m = [u.Unit_kg, u.Unit_g, u.Unit_Ms, u.Unit_Msol, u.Unit_Mg, 
        u.Unit_Mt, u.Unit_Mj]

    #  So far unused:
    #  u_v = [u.Unit_kmh, u.Unit_kms, u.Unit_ms]
    #
    #  Unit_mol = Units('mol')
    #  Unit_C = Units('C')
    #  Unit_K = Units('K')
    #
    #  Unit_ms2 = Unit_ms**2
    #  Unit_J = Unit_kg * Unit_ms2
    #  Unit_erg = Unit_g * (Unit_cm / Unit_s)**2
    #  Unit_N = Unit_kg * Unit_m / (Unit_s)**2
    #  Unit_Pa = Unit_N / Unit_m**2
    #  Unit_G = Unit_N * Unit_m**2 / Unit_kg**2
    #  Unit_d = Unit_kg / Unit_m**3
    #  Unit_Lsol = 3.839 * Unit_erg / Unit_s

    lsu = u.UnitSystem('local', 
        [bigrand()*r.choice(u_l), bigrand()*r.choice(u_m), bigrand()*r.choice(u_t)])

    return lsu


def hprint(*objects):
    """
    print without newline.
    """
    print(*objects, end=' ')
    return

def getArr(value=None, l=100):
    """
    get a numpy array of length l.
    If value is specified, every element will have that value.
    Otherwise, it will be random.
    """
    import numpy as np
    if value is None:
        arr = np.array([r.uniform(0, 100000)*(-1)**r.randint(0,2) for i in range(l)])
    else:
        arr = np.ones(l) * value

    return arr

def getPosArr(value=None, l=100):
    """
    get a numpy array of length l.
    If value is specified, every element will have that value.
    Otherwise, it will be random, BUT POSITIVE VALUES ONLY.
    """
    import numpy as np
    if value is None:
        arr = np.array([r.uniform(0,100000) for i in range(l)])
    else:
        arr = np.ones(l) * value

    return arr
