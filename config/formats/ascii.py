###########################################################################################
#  package:   pNbody
#  file:      ascii.py
#  brief:
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

import numpy as np

class Nbody_ascii:
    """
    This class defines the simple ascii format
    """

    def _init_spec(self):
        pass

    def get_excluded_extension(self):
        """
        Return a list of file to avoid when extending the default class.
        """
        return []

    def get_read_fcts(self):
        """
        returns the functions needed to read a file.
        """
        return [self.read_particles]

    def get_write_fcts(self):
        """
        returns the functions needed to write a file.
        """
        return [self.write_particles]

    def read_particles(self, f):
        """
        Function that read particles.
        """
        from pNbody import iofunc as pnio
        x, y, z, vx, vy, vz, m = pnio.read_ascii(f, list(range(7)))
        self.pos = np.transpose(np.array([x, y, z])).astype(np.float32)
        self.vel = np.transpose(np.array([vx, vy, vz])).astype(np.float32)
        self.mass = m.astype(np.float32)

    def write_particles(self, f):
        """
        Function that write particles.
        """
        for i in range(self.nbody):
            line = "%g %g %g %g %g %g %g \n" % (self.pos[i][0],
                                                self.pos[i][1],
                                                self.pos[i][2],
                                                self.vel[i][0],
                                                self.vel[i][1],
                                                self.vel[i][2],
                                                self.mass[i])
            f.write(line)
