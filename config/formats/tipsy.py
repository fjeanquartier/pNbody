###########################################################################################
#  package:   pNbody
#  file:      tipsy.py
#  brief:     tipsy file format
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################


import numpy as np

class Nbody_tipsy:

    def _init_spec(self):
        pass

    def get_excluded_extension(self):
        """
        Return a list of file to avoid when extending the default class.
        """
        return []

    def read_p(self, f):
        """
        specific format for particle file
        """
        # header
        h1 = np.fromstring(f.read(8), np.float)
        h2 = np.fromstring(f.read(4 * 5), np.int)
        tnow = h1[0]
        nbody = h2[0]
        ndim = h2[1]
        ngas = h2[2]
        ndark = h2[3]
        nstar = h2[4]

        # gas
        if ngas != 0:
            vec = np.fromstring(f.read(4 * 12 * ngas), np.float32)
            vec.shape = (ngas, 12)
            mass = vec[:, 0]
            pos = vec[:, 1:4]
            vel = vec[:, 4:7]

        # dark
        if ndark != 0:
            vec = np.fromstring(f.read(4 * 9 * ndark), np.float32)
            vec.shape = (ndark, 9)

            if ngas == 0:
                mass = vec[:, 0]
                pos = vec[:, 1:4]
                vel = vec[:, 4:7]
            else:
                mass = np.concatenate((mass, vec[:, 0]))
                pos = np.concatenate((pos, vec[:, 1:4]))
                vel = np.concatenate((vel, vec[:, 4:7]))

        # stars
        if nstar != 0:
            vec = np.fromstring(f.read(4 * 11 * nstar), np.float32)
            vec.shape = (nstar, 11)

            if ngas == 0 and ndark == 0:
                mass = vec[:, 0]
                pos = vec[:, 1:4]
                vel = vec[:, 4:7]
            else:
                mass = np.concatenate((mass, vec[:, 0]))
                pos = np.concatenate((pos, vec[:, 1:4]))
                vel = np.concatenate((vel, vec[:, 4:7]))

        # make global
        self.tnow = tnow
        self.nbody = nbody
        self.label = 'tipsy file'

        self.pos = pos
        self.vel = vel
