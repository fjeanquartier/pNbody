#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      findmax.py
#  brief:     Find max radius
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

import sys
from pNbody import *

file = sys.argv[1]

nb = Nbody(file, ftype='gadget', pio='yes')
local_max = max(nb.rxyz())
global_max = mpi.mpi_max(nb.rxyz())

print(("proc %d local_max = %f global_max = %f" %
       (mpi.ThisTask, local_max, global_max)))
