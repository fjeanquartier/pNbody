#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      testparallel.py
#  brief:     test parallel io
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from pNbody import *

ftype = 'gadget'
file = 'gadget_file.dat'


#################################
#
# open/save	(pio=no)
#
#################################

nb = Nbody(file, ftype=ftype, pio='no')
nb.rename('copy.dat')
nb.write()


#################################
#
# save	(pio=yes)
#
#################################

nb.set_pio('yes')
nb.write()


#################################
#
# open	(pio=yes)
#
#################################

nb = Nbody('copy.dat', ftype=ftype, pio='yes')
nb.set_pio('no')
nb.rename('copy2.dat')
nb.write()
