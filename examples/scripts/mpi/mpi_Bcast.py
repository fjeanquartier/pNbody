#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_Bcast.py
#  brief:     Broadcast messages
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################



from pNbody import mpiwrapper as mpi

if mpi.mpi_IsMaster():
    msg = "hello from the master"
else:
    msg = "hello from a slave"

msg = mpi.mpi_bcast(msg, 0)
mpi.mpi_iprint(msg)
