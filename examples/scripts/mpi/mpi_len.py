#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_len.py
#  brief:     Compute len of array
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################



from pNbody import *
import numpy as np

if mpi.mpi_IsMaster():
    x = np.arange(0, 3)
else:
    x = np.arange(3, 6)

mpi.mpi_iprint(x)
l = mpi.mpi_len(x)

mpi.mpi_iprint("len= %g " % l)
