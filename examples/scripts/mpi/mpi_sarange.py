#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      mpi_snp.arange.py
#  brief:     np.arange
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

from pNbody import *
from numpy import array

nall = np.array([[2, 3], [5, 1]])

x = mpi.mpi_snp.arange(nall)
mpi.mpi_iprint(x)
