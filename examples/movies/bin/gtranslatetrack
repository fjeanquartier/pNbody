#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      gtranslatetrack
#  brief:     Image example
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################
'''
Convert glups .trk files into gwinparameters file

Yves Revaz
mer oct 25 10:27:24 CEST 2006
'''


import sys
import string
import os
import types
from optparse import OptionParser

from pylab import *


from numpy import *
#from Nbody import myNumeric
from Gtools import io
import Ptools as pt


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-n",
                      action="store",
                      dest="Nout",
                      type="int",
                      default=10,
                      help="number of final frames",
                      metavar=" INT")

    parser.add_option("--basename",
                      action="store",
                      dest="basename",
                      type="string",
                      default='newtrack',
                      help="image base name",
                      metavar=" STRING")

    parser.add_option("--dirname",
                      action="store",
                      dest="dirname",
                      type="string",
                      default='track',
                      help="image directory name",
                      metavar=" STRING")

    parser.add_option("-p",
                      action="store",
                      dest="parameterfile",
                      type="string",
                      default=None,
                      help="""parameter file : allows to force some parameters
		   the file must constains some commands like :
                   forceDic["Points0:Color_a"] = 0.1
		   forceDic["Points1:Color_a"] = 0.1
		   forceDic["Points5:Color_a"] = 0.7
		   """,
                      metavar=" FILE")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print("you must specify at least a filename")
        sys.exit(0)

    files = args

    return files, options

#######################################
# GET OPTIONS
#######################################


# get options
files, options = parse_options()
Nout = options.Nout
basename = options.basename
parameterfile = options.parameterfile
dirname = options.dirname


trackfile = files[0]
xyzfile = files[1]


# read cms (center of mass splined)
t, a, x, y, z = pt.io.read_ascii(xyzfile, [0, 1, 2, 3, 4])


# read track (center of mass splined)
elts = io.read_track(trackfile)


##################################
# main loop
##################################

for i in range(len(t)):

    newelts = elts
    dx = x[i] - newelts["Observer_0:P0"]
    dy = y[i] - newelts["Observer_0:P1"]
    dz = z[i] - newelts["Observer_0:P2"]

    newelts["Observer_0:M0"] = newelts["Observer_0:M0"] + dx
    newelts["Observer_0:M1"] = newelts["Observer_0:M1"] + dy
    newelts["Observer_0:M2"] = newelts["Observer_0:M2"] + dz

    newelts["Observer_0:M4"] = newelts["Observer_0:M4"] + dx
    newelts["Observer_0:M5"] = newelts["Observer_0:M5"] + dy
    newelts["Observer_0:M6"] = newelts["Observer_0:M6"] + dz

    newelts["Observer_0:M8"] = newelts["Observer_0:M8"] + dx
    newelts["Observer_0:M9"] = newelts["Observer_0:M9"] + dy
    newelts["Observer_0:M10"] = newelts["Observer_0:M10"] + dz

    newelts["Observer_0:M12"] = newelts["Observer_0:M12"] + dx
    newelts["Observer_0:M13"] = newelts["Observer_0:M13"] + dy
    newelts["Observer_0:M14"] = newelts["Observer_0:M14"] + dz

    newelts["Observer_0:P0"] = newelts["Observer_0:P0"] + dx
    newelts["Observer_0:P1"] = newelts["Observer_0:P1"] + dy
    newelts["Observer_0:P2"] = newelts["Observer_0:P2"] + dz

    #file = os.path.basename(trackfile)+".%04d"%(i)
    file = "newtrack/snapshot_%04d.selection.00000" % (i)

    f = open(file, 'w')
    keys = sorted(newelts.keys())

    for key in keys:

        if isinstance(newelts[key], types.ListType):
            line = "%s = %s\n" % (key, newelts[key])
        else:
            line = "%s = %f\n" % (key, newelts[key])

        f.write(line)

    f.close()
