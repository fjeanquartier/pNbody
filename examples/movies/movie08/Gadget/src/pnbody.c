#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#include <Python.h>
#include <numpy/arrayobject.h>

#include "allvars.h"
#include "proto.h"

#ifdef PNBODY



double NextTime;

PyObject *ImageModule;
PyObject *ImageDict;
PyObject *ImageMaker;
PyObject *pyCreator;
PyObject *pyArgs;
PyObject *pyFile;
PyObject *pyNextTime;

#ifdef PNBODY_OUTPUT_POS
PyArrayObject *Pos;
#endif

#ifdef PNBODY_OUTPUT_VEL
PyArrayObject *Vel;
#endif

#ifdef PNBODY_OUTPUT_NUM
PyArrayObject *Num;
#endif

#ifdef PNBODY_OUTPUT_MASS
PyArrayObject *Mass;
#endif

#ifdef PNBODY_OUTPUT_TYPE
PyArrayObject *Tpe;	
#endif

#ifdef PNBODY_OUTPUT_ENERGY
PyArrayObject *U;
#endif

#ifdef PNBODY_OUTPUT_DENSITY
PyArrayObject *Rho;
#endif

#ifdef PNBODY_OUTPUT_HSML
PyArrayObject *Rsp;
#endif

#ifdef PNBODY_OUTPUT_METALS
PyArrayObject *Metals;
#endif





PyObject * HeaderDict;
 
#ifdef CHIMIE_EXTRAHEADER 
PyObject *SolarAbundancesDict;
#endif  
 
void init_pnbody()
{

  /* initialize python and numpy */
  Py_Initialize();
  import_array();

  ImageModule = PyImport_ImportModule("Mkgmov");


  /* create the object */
  pyCreator = PyObject_GetAttrString(ImageModule, "Movie");
  
  /* file */
  pyFile = PyString_FromString("filmparam.py");
  pyArgs = PyTuple_New(1);
  PyTuple_SetItem(pyArgs, 0, pyFile);
  
  //ImageMaker = PyObject_CallObject(pyCreator, NULL);
  ImageMaker = PyObject_CallObject(pyCreator, pyArgs);


  /* deallocate */
  Py_DECREF(pyCreator);
  Py_DECREF(pyArgs);
  Py_DECREF(pyFile);

  /* dictionary */
  ImageDict =PyDict_New();
  HeaderDict =PyDict_New();



#ifdef CHIMIE_EXTRAHEADER
  int i;
  SolarAbundancesDict =PyDict_New();

  for (i=0;i<get_nelts();i++)
    PyDict_SetItemString(SolarAbundancesDict,get_Element(i), PyFloat_FromDouble((double)get_SolarAbundance(i)));

#endif  
 





  /* give some info */
  PyObject_CallMethod(ImageMaker, "info",NULL);

}




/*! finalize pNbody 
 *
 */
void finalize_pnbody()
{

 
  /* clean */
  Py_DECREF(ImageDict);
  Py_DECREF(HeaderDict);
  Py_DECREF(ImageMaker);
  Py_DECREF(SolarAbundancesDict);
    

  Py_Finalize();  
    
}



void compute_pnbody()
{


  /****************************************/
  /* check if it is time for the next image
  /****************************************/
   
  pyNextTime = PyObject_CallMethod(ImageMaker, "get_next_time",NULL);  

  if (!PyFloat_Check(pyNextTime))
    {
      printf("no more time left.\n");
      Py_DECREF(pyNextTime);
      return;
    }
      
  NextTime = PyFloat_AsDouble(pyNextTime);    
    
  printf("  Time=%g NextTime=%g\n",All.Time,NextTime);
  
  
  if (NextTime<=All.Time)
    {

      /* set next time */ 
      PyObject_CallMethod(ImageMaker, "set_next_time",NULL);

      /***********************************/
      /* create arrays
      /***********************************/
      
      int i,k;
      npy_intp ld1[2],ld2[1],ldn[2];	
//    FLOAT Ui,Rhoi,Rspi;
  
  
      ld2[0]=NumPart;
      ld2[1]=3;
      
      ld1[0]=NumPart;
      
      ldn[0]=NumPart;
      ldn[1]=NELEMENTS;      
      
      
#ifdef PNBODY_OUTPUT_POS   
      Pos = (PyArrayObject *) PyArray_SimpleNew(2,ld2,NPY_FLOAT);
#endif      
#ifdef PNBODY_OUTPUT_VEL       
      Vel = (PyArrayObject *) PyArray_SimpleNew(2,ld2,NPY_FLOAT);
#endif      
#ifdef PNBODY_OUTPUT_NUM        
      Num = (PyArrayObject *) PyArray_SimpleNew(1,ld1,NPY_INT);
#endif      
#ifdef PNBODY_OUTPUT_MASS       
      Mass= (PyArrayObject *) PyArray_SimpleNew(1,ld1,NPY_FLOAT);
#endif      
#ifdef PNBODY_OUTPUT_TYPE       
      Tpe = (PyArrayObject *) PyArray_SimpleNew(1,ld1,NPY_INT);
#endif      
#ifdef PNBODY_OUTPUT_ENERGY      
      U   = (PyArrayObject *) PyArray_SimpleNew(1,ld1,NPY_FLOAT);
#endif      
#ifdef PNBODY_OUTPUT_DENSITY        
      Rho = (PyArrayObject *) PyArray_SimpleNew(1,ld1,NPY_FLOAT);
#endif      
#ifdef PNBODY_OUTPUT_HSML       
      Rsp = (PyArrayObject *) PyArray_SimpleNew(1,ld1,NPY_FLOAT);
#endif      
#ifdef PNBODY_OUTPUT_METALS        
      Metals = (PyArrayObject *) PyArray_SimpleNew(2,ldn,NPY_FLOAT);
#endif  
 
  
      for (i=0;i<NumPart;i++)
        {
#ifdef PNBODY_OUTPUT_POS           
          *(float*)(Pos->data + i*(Pos->strides[0]) + 0*Pos->strides[1]) = P[i].Pos[0] ;
          *(float*)(Pos->data + i*(Pos->strides[0]) + 1*Pos->strides[1]) = P[i].Pos[1] ;
          *(float*)(Pos->data + i*(Pos->strides[0]) + 2*Pos->strides[1]) = P[i].Pos[2] ;
#endif
#ifdef PNBODY_OUTPUT_VEL 	  
          *(float*)(Vel->data + i*(Vel->strides[0]) + 0*Vel->strides[1]) = P[i].Vel[0] ;
          *(float*)(Vel->data + i*(Vel->strides[0]) + 1*Vel->strides[1]) = P[i].Vel[1] ;
          *(float*)(Vel->data + i*(Vel->strides[0]) + 2*Vel->strides[1]) = P[i].Vel[2] ;
#endif
#ifdef PNBODY_OUTPUT_NUM
	  *(int*)  (Num->data + i*(Num->strides[0]))   = P[i].ID;
#endif	
#ifdef PNBODY_OUTPUT_TYPE 
	  *(int*)  (Tpe->data + i*(Tpe->strides[0]))   = P[i].Type;
#endif	
#ifdef PNBODY_OUTPUT_MASS	  
	  *(float*)(Mass->data + i*(Mass->strides[0])) = P[i].Mass;
#endif	  


	  
//	  if (i<N_gas)
//	    {
//	    
//#ifdef ISOTHERM_EQS	    
//	      Ui   = P[i].Entropy ;
//#else
//              Ui   = dmax(All.MinEgySpec,SphP[i].Entropy / GAMMA_MINUS1 * pow(SphP[i].Density , GAMMA_MINUS1));
//	      //Ui   = dmax(All.MinEgySpec,SphP[i].Entropy / GAMMA_MINUS1 * pow(SphP[i].Density * a3inv, GAMMA_MINUS1));
//#endif	      
//	      Rhoi =SphP[i].Density ;
//	      
//	      Rspi = SphP[i].Hsml ;
//	    }
//	  else
//	    {
//	      Ui = 0;
//	      Rhoi=0;
//	      Rspi=0;
//	    }  
//#ifdef PNBODY_OUTPUT_ENERGY
//	  *(float*)(U->data   + i*(  U->strides[0]))   = Ui ;
//#endif	  
//#ifdef PNBODY_OUTPUT_DENSITY	  
//	  *(float*)(Rho->data + i*(Rho->strides[0]))   = Rhoi ;
//#endif	 
//#ifdef PNBODY_OUTPUT_HSML	  
//	  *(float*)(Rsp->data + i*(Rsp->strides[0]))     = Rspi ;  
//#endif	





#ifdef PNBODY_OUTPUT_ENERGY
          switch (P[i].Type)
            {
	      case 0:
#ifdef ISOTHERM_EQS	
                *(float*)(U->data   + i*(  U->strides[0]))   = SphP[i].Entropy;
#else	      
                *(float*)(U->data   + i*(  U->strides[0]))   = dmax(All.MinEgySpec,SphP[i].Entropy / GAMMA_MINUS1 * pow(SphP[i].Density , GAMMA_MINUS1));
		// *(float*)(U->data   + i*(  U->strides[0]))   = dmax(All.MinEgySpec,SphP[i].Entropy / GAMMA_MINUS1 * pow(SphP[i].Density* a3inv , GAMMA_MINUS1));
#endif		
		break;
	      default:
	        *(float*)(U->data   + i*(  U->strides[0]))   = 0.;
	        break;
            }
#endif

#ifdef PNBODY_OUTPUT_DENSITY
          switch (P[i].Type)
            {
	      case 0:
                *(float*)(Rho->data + i*(Rho->strides[0])) = SphP[i].Density;
		break;
	      default:
	        *(float*)(Rho->data + i*(Rho->strides[0])) = 0.;
	        break;
            }
#endif


#ifdef PNBODY_OUTPUT_HSML
          switch (P[i].Type)
            {
	      case 0:
                *(float*)(Rsp->data + i*(Rsp->strides[0])) = SphP[i].Hsml;
		break;
	      default:
	        *(float*)(Rsp->data + i*(Rsp->strides[0])) = 0.;
	        break;
            }
#endif



#ifdef PNBODY_OUTPUT_METALS
          switch (P[i].Type)
            {
	      case 0:
	        for(k=0;k<NELEMENTS;k++)
	          *(float*)(Metals->data + i*(Metals->strides[0]) + k*Metals->strides[1]) = SphP[i].Metal[k];
	        break;
	      
	      case ST:
	        for(k=0;k<NELEMENTS;k++)
	          *(float*)(Metals->data + i*(Metals->strides[0]) + k*Metals->strides[1]) = StP[P[i].StPIdx].Metal[k];
		 break;  
              
	      default:
	        for(k=0;k<NELEMENTS;k++)
	          *(float*)(Metals->data + i*(Metals->strides[0]) + k*Metals->strides[1]) = 0;	        
	        break;
	    }
#endif

	  
        }      
  

      /***********************************/
      /* header
      /***********************************/
      PyDict_Clear(HeaderDict);
      PyDict_SetItemString(HeaderDict,"flag_chimie_extraheader",  PyFloat_FromDouble((int)header.flag_chimie_extraheader));
      PyDict_SetItemString(HeaderDict,"flag_metals",              PyFloat_FromDouble((int)header.flag_metals));
  
  


  
  
      /***********************************/
      /* create image
      /***********************************/

  

      /* fill the dictionnary */
      PyDict_Clear(ImageDict);
      
      PyDict_SetItemString(ImageDict,"header",  (PyObject*)HeaderDict);
      PyDict_SetItemString(ImageDict,"atime", PyFloat_FromDouble((double)All.Time));



#ifdef PNBODY_OUTPUT_POS      
      PyDict_SetItemString(ImageDict,"pos",  (PyObject*)Pos);
#endif      
#ifdef PNBODY_OUTPUT_VEL      
      PyDict_SetItemString(ImageDict,"vel",  (PyObject*)Vel);
#endif      
#ifdef PNBODY_OUTPUT_NUM      
      PyDict_SetItemString(ImageDict,"num",  (PyObject*)Num);
#endif      
#ifdef PNBODY_OUTPUT_MASS      
      PyDict_SetItemString(ImageDict,"mass", (PyObject*)Mass);
#endif      
#ifdef PNBODY_OUTPUT_TYPE      
      PyDict_SetItemString(ImageDict,"tpe",  (PyObject*)Tpe);
#endif      
#ifdef PNBODY_OUTPUT_ENERGY      
      PyDict_SetItemString(ImageDict,"u",    (PyObject*)U);
#endif      
#ifdef PNBODY_OUTPUT_DENSITY      
      PyDict_SetItemString(ImageDict,"rho",  (PyObject*)Rho);
#endif      
#ifdef PNBODY_OUTPUT_HSML      
      PyDict_SetItemString(ImageDict,"rsp",  (PyObject*)Rsp);    
#endif        
#ifdef PNBODY_OUTPUT_METALS      
      PyDict_SetItemString(ImageDict,"metals",  (PyObject*)Metals); 
#endif      
      


#ifdef CHIMIE_EXTRAHEADER  
      PyDict_SetItemString(ImageDict,"nelts",  PyInt_FromLong((long)get_nelts()));
      PyDict_SetItemString(ImageDict,"ChimieSolarAbundances",  (PyObject*)SolarAbundancesDict);
#endif



      /* call the dump method */
      PyObject_CallMethod(ImageMaker, "dump","O",ImageDict);
  
  


#ifdef PNBODY_OUTPUT_POS
      Py_DECREF(Pos);
#endif

#ifdef PNBODY_OUTPUT_VEL
      Py_DECREF(Vel);
#endif

#ifdef PNBODY_OUTPUT_NUM
      Py_DECREF(Num);
#endif

#ifdef PNBODY_OUTPUT_MASS
      Py_DECREF(Mass);
#endif

#ifdef PNBODY_OUTPUT_TYPE
      Py_DECREF(Tpe);    	
#endif

#ifdef PNBODY_OUTPUT_ENERGY
      Py_DECREF(U);
#endif

#ifdef PNBODY_OUTPUT_DENSITY
      Py_DECREF(Rho);
#endif

#ifdef PNBODY_OUTPUT_HSML
      Py_DECREF(Rsp);
#endif

#ifdef PNBODY_OUTPUT_METALS
      Py_DECREF(Metals);
#endif



  }
  
  Py_DECREF(pyNextTime);

}




#endif

