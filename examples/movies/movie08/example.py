#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      example.py
#  brief:     Example
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################

import Mkgmov
from pNbody import ic

movie = Mkgmov.Movie('filmparam.py')


for i in range(100):
    nb = ic.box(1000, 1, 1, 1, ftype='gadget')
    # now create an image
    movie.dumpimage(nb)
