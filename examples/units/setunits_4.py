#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      setunits_4.py
#  brief:     Units conversion
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of pNbody.
###########################################################################################

from pNbody import *

nb = Nbody("dsph.dat", ftype='gadget', unitsfile='params.dsph')

nb.localsystem_of_units.info()


# trivial conversion factor into kpc,10**10Msol,Myr
print(
    "to km/s    : *",
    nb.localsystem_of_units.convertionFactorTo(
        units.Unit_kms))
print(
    "to g/cm**3 : *",
    nb.localsystem_of_units.convertionFactorTo(
        units.Unit_g /
        units.Unit_cm**3))
print(
    "to Msol    : *",
    nb.localsystem_of_units.convertionFactorTo(
        units.Unit_Msol))
print(
    "to Msol/yr : *",
    nb.localsystem_of_units.convertionFactorTo(
        units.Unit_Msol /
        units.Unit_yr))
