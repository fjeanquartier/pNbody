#!/bin/bash

autopep8 -r -j -1 --in-place --aggressive --aggressive config Doc examples fonts MpiTest pNbody scripts/* src test setup.py
clang-format -style=file -i src/*/*.[ch]
