#!/usr/bin/env python3
'''
 @package   pNbody
 @file      setup.py
 @brief     Install pNbody
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from setuptools import setup, find_packages, Extension
from glob import glob
from copy import deepcopy
import numpy as np


# optional modules
options = {
    "ptreelib": False,
    "cooling_with_metals": True,
    "pygsl": True,
}

# special flags for modules
spec_flags = {
    # "logger_loader": ["-Wall", "-Wextra", "-DSWIFT_DEBUG_CHECKS"]
}

# default flags
flags = ["-Werror", "-Wno-unused-parameter"]

# directory containing headers
include_dirs = [
    ".",
    np.get_include()
]

# special includes
spec_inc = {
    "ptreelib": ["$MPICH_ROOT/include",
                 "src/ptreelib"],
}

# libraries
libraries = ["m"]

# special libraries
spec_lib = {
    "ptreelib": ["mpich"],
    "cooling_with_metals": ["gsl", "gslcblas"],
    "pygsl": ["gsl"]
}

# special libraries directory
spec_lib_dir = {
    "ptreelib": ["$MPICH_ROOT/lib"]
}

# C modules
modules_name = [
    "nbodymodule",
    "myNumeric",
    "mapping",
    "montecarlolib",
    "iclib",
    "treelib",
    "nbdrklib",
    "peanolib",
    "coolinglib",
    "cosmolib",
    "asciilib",
    "tessel",
    #"libtipsy",    crash which fedora 23: needs CFLAGS="-I/usr/include/tirpc" CXXFLAGS="-I/usr/include/tirpc"
    "thermodynlib",
    "orbitslib"
]

# data files
data_files = []

data_files.append(('config', glob('./config/*parameters')))
data_files.append(('config/rgb_tables', glob('./config/rgb_tables/*')))
# # trick to avoid rpm problems
data_files.append(('config/formats', glob('./config/formats/*.py')))
data_files.append(('config/extensions', glob('./config/extensions/*.py')))
data_files.append(('plugins', glob('./config/*.py')))
data_files.append(('plugins', glob('./config/plugins/*.py')))
data_files.append(('config/opt/SSP', glob('./config/opt/SSP/*[txt,dat]')))
data_files.append(('config/opt/SSP/P94_salpeter',
                   glob('./config/opt/SSP/P94_salpeter/*')))
data_files.append(('fonts', glob('./fonts/*')))

# examples
data_files.append(('examples', glob('./examples/*.dat')))
data_files.append(('examples', glob('./examples/*.py')))
data_files.append(('examples/scripts', glob('./examples/scripts/*.py')))

# examples ic
data_files.append(('examples/ic', glob('./examples/ic/*.py')))
data_files.append(('examples/ic', glob('./examples/ic/Readme')))

# DO NOT TOUCH BELOW

# add options
for k in options.keys():
    if options[k]:
        modules_name.append(k)

# Generate extensions
ext_modules = []

for k in modules_name:
    # get flags
    extra_flags = deepcopy(flags)
    if k in spec_flags:
        extra_flags.extend(spec_flags[k])
    # get libraries
    extra_lib = deepcopy(libraries)
    if k in spec_lib:
        extra_lib.extend(spec_lib[k])
    # get library directories
    extra_lib_dirs = []
    if k in spec_lib_dir:
        extra_lib_dirs.extend(spec_lib_dir[k])

    # compile extension
    tmp = Extension("pNbody." + k,
                    glob("src/" + k + "/*.c"),
                    include_dirs=include_dirs,
                    libraries=extra_lib,
                    extra_compile_args=extra_flags,
                    library_dirs=extra_lib_dirs)

    ext_modules.append(tmp)


# scripts to install
scripts = glob("scripts/*")

setup(
    name="pNbody",
    author="Yves Revaz",
    author_email="yves.revaz@epfl.ch",
    url="http://obswww.unige.ch/~revaz/pNbody/index.html",
    description="""
    This module provides lots of tools used
    to deal with Nbody particles models.
    """,
    license="GPLv3",
    version="5.0",

    packages=find_packages(),
    scripts=scripts,
    ext_modules=ext_modules,
    data_files=data_files,
    install_requires=["numpy", "scipy", "h5py",
                      "astropy", "pillow"],

)
