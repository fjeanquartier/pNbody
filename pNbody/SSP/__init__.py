#!/usr/bin/env python3

###########################################################################################
#  package:   pNbody
#  file:      __init__.py
#  brief:     luminosities files
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
###########################################################################################
