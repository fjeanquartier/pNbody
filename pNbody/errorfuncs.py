#!/usr/bin/env python3
###########################################################################################
#  package:   pNbody
#  file:      errorfuncs.py
#  brief:     Defines a few error classes
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################


class pNbodyError(Exception):
    """
    a simple new error
    """
    pass


class FormatError(Exception):
    """
    format error
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def todo_warning(verb=2):
    """
    Print a short warning and traceback for empty functions.

    Parameters:
    -----------

    verb: verbosity level as defied in class Nbody() in main.py
    """
    import traceback

    if verb == 0:
        return

    else:
        print("=================================================================================")
        print("TODO WARNING: A function that has not been fully written has been called.")
        print("TODO WARNING: This is most likely no reason to worry, but hopefully this message ")
        print("TODO WARNING: will annoy the developers enough to start clean their code up.")
        print("TODO WARNING: Meanwhile, you can just carry on doing whatever you were up to.")

        if verb > 1:
            print("TODO WARNING: Printing traceback:")

            st = traceback.extract_stack()
            reduced_stack = st[:-1]  # skip call to traceback.extract_stack() in stack trace
            traceback.print_list(reduced_stack)

            print("TODO WARNING: End of (harmless) warning.")
        
        print("=================================================================================")
        
    return
