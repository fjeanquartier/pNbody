the pyfits module
**********************

.. currentmodule:: pNbody.pyfits

.. automodule:: pNbody.pyfits
   :members:
