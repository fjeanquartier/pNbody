the C PyGadget module
**************************

.. warning::

   The C PyGadget module is deprecated. You should use the PyGear module
   which is included in the `gear <https://gitlab.com/revaz/Gear>`__ package.

This module is currently not completely integrated to **pNbody**.
It is part of the **pNbody** package but must be compiled
separately.
For mpi, use:: 
  
  export CC=mpirun
  
  

.. currentmodule:: PyGadget.gadget

.. automodule:: PyGadget.gadget
   :members:
