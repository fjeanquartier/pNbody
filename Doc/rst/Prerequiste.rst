Prerequiste
**********************

The basic module of pNbody needs python and additional packages :

1) Python 2.5.x, 2.6.x, 2.7.x

   http://www.python.org

2) a C compiler 

   gcc is fine http://gcc.gnu.org/

3) numpy-1.0.4 or higher
   
   http://numpy.scipy.org/

4) Imaging 1.1.5 or higher
   
   http://www.pythonware.com/products/pil/


For additional but usefull special functions :

5) scipy 0.7 or higher
   
   http://www.scipy.org/


For the parallel capabilities, an mpi distribution is needed (ex. openmpi) 
as well as the additional python mpi wrapping:

6) mpi4py
   http://cheeseshop.python.org/pypi/mpi4py
 
In order to convert movies in standard format (gif or mpeg), the two following applications are needed :

1) convert (imagemagick)
   
   http://www.imagemagick.org/script/index.php
   
2) mencoder (mplayer)
   
   http://www.mplayerhq.hu/design7/news.html
