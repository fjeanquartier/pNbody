the C cosmolib module
**********************

.. currentmodule:: pNbody.cosmolib

.. automodule:: pNbody.cosmolib
   :members:
