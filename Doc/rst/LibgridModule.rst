the libgrid module
**********************

.. currentmodule:: pNbody.libgrid

.. automodule:: pNbody.libgrid
   :members:
