
[![build status](https://gitlab.com/revaz/pNbody/badges/master/build.svg)](https://gitlab.com/revaz/pNbody/commits/master)
[![coverage report](https://gitlab.com/revaz/pNbody/badges/master/coverage.svg)](https://gitlab.com/revaz/pNbody/commits/master)

--------------------------------------------------------------------
Requirements
--------------------------------------------------------------------
  
  * C compiler
  * Python 3.6 or higher
  * numpy, scipy, h5py and astropy
  * PIL (the Python Image Library module with python-imaging-tk enabled)
  
--------------------------------------------------------------------
Build instructions for UNIX/LINUX
--------------------------------------------------------------------


2. Using Python 3.6 or later, you can use the setup.py
   script to build the module.  The following commands should do
   the trick:

        $ python setup.py build
        $ python setup.py install

   (depending on how Python has been installed on your machine,
   you might have to log in as a superuser to run the 'install'
   command or use the user parameter.)

   If the setup.py command finishes without any errors, you're
   done!


	
--------------------------------------------------------------------
Build instructions for Windows
--------------------------------------------------------------------

1. Install Linux on your Windows machine and follow the 
   instructions for UNIX



--------------------------------------------------------------------	
Documentation
--------------------------------------------------------------------
	
   The documentation is hosted here http://obswww.unige.ch/~revaz/pNbody/index.html

	
--------------------------------------------------------------------	
Getting Help
--------------------------------------------------------------------

  If you have questions about python Nbody module, feel free to, write 
  an issue:
