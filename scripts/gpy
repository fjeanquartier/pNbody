#!/usr/bin/env python3 -i
###########################################################################################
#  package:   pNbody
#  file:      gpy
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of pNbody.
###########################################################################################

import os
import _thread
import sys
import string
import time
import glob

from tkinter import *
from tkinter.messagebox import askokcancel
from tkinter.messagebox import showerror
from tkinter.filedialog import asksaveasfilename
from tkinter.filedialog import askopenfilename

from numpy import *

from pNbody import *
from pNbody import param
from pNbody import geometry as geo

import Ptools as pt

try:
    from optparse import OptionParser
except ImportError:
    from optik import OptionParser


########################################
#
# parser
#
########################################


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser = pt.add_units_options(parser)
    parser = pt.add_ftype_options(parser)

    parser.add_option("--exec",
                      action="store",
                      dest="execline",
                      type="string",
                      default=None,
                      help="give command to execute before",
                      metavar=" STRING")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        # print "you must specify a filename"
        # sys.exit(0)
        file = None
    else:
        file = args

    return file, options


##########################################################################
#
#                                    MAIN
#
##########################################################################


file, options = parse_options()

execline = options.execline


nb = Nbody(file, ftype=options.ftype)


# define local units
unit_params = pt.do_units_options(options)
nb.set_local_system_of_units(params=unit_params)


if execline is not None:
    exec(execline)
