#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>





struct Point
  {
    double Pos[3];
  };



struct TriangleInList
  {

    int idx;            		/* index of current triangle */
 
    struct Point *Pt1;		       /* pointer towards the first  point */
    struct Point *Pt2;		       /* pointer towards the second point */
    struct Point *Pt3;		       /* pointer towards the third  point */
    struct TriangleInList *T1;	       /* index of first  triangle */
    struct TriangleInList *T2;	       /* index of second triangle */
    struct TriangleInList *T3;	       /* index of third  triangle */
 
  };


struct TriangleInList2
  {
    int idx;            		/* index of current triangle */
    struct Point *P[3];              	/* 3 points*/
    struct TriangleInList *T[3];      	/* 3 addtriangles */
    int idxe[3];			/* index of opposite points */
 
  };





int main(void)
  {
    
    double pos[3];
    struct Point Pe[5];
    struct TriangleInList Tl[4];
    struct TriangleInList T1,T2,T3;
    struct TriangleInList *Te;
    
    struct TriangleInList2 Tl2[4];
    
    int i,i0,i1,i2;


    i = 2;
    
    i0= i;
    i1= (i+1) % 3;
    
    printf("%d %d\n",i0,i1);
    
    exit(-1);

    
    i = 2;
    
    i0= i;
    i1= (i+1) % 3;
    i2= (i+2) % 3;
    
    printf("%d %d %d\n",i0,i1,i2);
    
    exit(-1);
    
    
       
    
    Pe[0].Pos[0] = 1;
    Pe[0].Pos[1] = 2;
    Pe[0].Pos[2] = 3;
  
    Pe[1].Pos[0] = 4;
    Pe[1].Pos[1] = 5;
    Pe[1].Pos[2] = 6;
    
    Pe[2].Pos[0] = 7;
    Pe[2].Pos[1] = 8;
    Pe[2].Pos[2] = 9;
    
    Pe[3].Pos[0] = -1;
    Pe[3].Pos[1] = -1;
    Pe[3].Pos[2] = -1;

    Pe[4].Pos[0] = -2;
    Pe[4].Pos[1] = -2;
    Pe[4].Pos[2] = -2;  
  
    T1.Pt1 =  &Pe[3];
    T1.Pt2 =  &Pe[3];
    T1.Pt3 =  &Pe[3];
  
    T2.Pt1 =  &Pe[4];
    T2.Pt2 =  &Pe[4];
    T2.Pt3 =  &Pe[4];  
    
  
    /* point towards a point */
    Tl[0].Pt1 =  &Pe[0];
    Tl[0].Pt2 =  &Pe[1];
    Tl[0].Pt3 =  &Pe[2];

    Tl[1].Pt1 =  &Pe[2];
    Tl[1].Pt2 =  &Pe[0];
    Tl[1].Pt3 =  &Pe[1];
    
    /* point towards a triangle */
    Tl[0].T1  = &T1;

    /* get a triangle */
    Tl[2]     = *Tl[0].T1;
    Tl[2].T1  = &Tl[0];


    Tl[3].Pt1 =  &Pe[1];
    Tl[3].Pt2 =  &Pe[2];
    Tl[3].Pt3 =  &Pe[0];
    Tl[3].T1  =  &T2;



    /* new struct */
    Tl2[0].P[0] =  &Pe[0];
    Tl2[0].P[1] =  &Pe[1];
    Tl2[0].P[2] =  &Pe[2];
   
    printf("new\n");  
    printf("Tl20\n");  
    printf("%g %g %g\n",Tl2[0].P[0]->Pos[0],Tl2[0].P[0]->Pos[1],Tl2[0].P[0]->Pos[2]);  
    printf("%g %g %g\n",Tl2[0].P[1]->Pos[0],Tl2[0].P[1]->Pos[1],Tl2[0].P[1]->Pos[2]);  
    printf("%g %g %g\n",Tl2[0].P[2]->Pos[0],Tl2[0].P[2]->Pos[1],Tl2[0].P[2]->Pos[2]);  
   
    
    printf("end new\n");




    printf("Tl0\n");  
    printf("%g %g %g\n",Tl[0].Pt1->Pos[0],Tl[0].Pt1->Pos[1],Tl[0].Pt1->Pos[2]);  
    printf("%g %g %g\n",Tl[0].Pt2->Pos[0],Tl[0].Pt2->Pos[1],Tl[0].Pt2->Pos[2]);  
    printf("%g %g %g\n",Tl[0].Pt3->Pos[0],Tl[0].Pt3->Pos[1],Tl[0].Pt3->Pos[2]);  
    printf("Tl1\n");
    printf("%g %g %g\n",Tl[1].Pt1->Pos[0],Tl[1].Pt1->Pos[1],Tl[1].Pt1->Pos[2]);  
    printf("%g %g %g\n",Tl[1].Pt2->Pos[0],Tl[1].Pt2->Pos[1],Tl[1].Pt2->Pos[2]);  
    printf("%g %g %g\n",Tl[1].Pt3->Pos[0],Tl[1].Pt3->Pos[1],Tl[1].Pt3->Pos[2]);  
    printf("Tl2\n");
    printf("%g %g %g\n",Tl[2].Pt1->Pos[0],Tl[2].Pt1->Pos[1],Tl[2].Pt1->Pos[2]);  
    printf("%g %g %g\n",Tl[2].Pt2->Pos[0],Tl[2].Pt2->Pos[1],Tl[2].Pt2->Pos[2]);  
    printf("%g %g %g\n",Tl[2].Pt3->Pos[0],Tl[2].Pt3->Pos[1],Tl[2].Pt3->Pos[2]);  
    printf("Tl3\n");
    printf("%g %g %g\n",Tl[3].Pt1->Pos[0],Tl[3].Pt1->Pos[1],Tl[3].Pt1->Pos[2]);  
    printf("%g %g %g\n",Tl[3].Pt2->Pos[0],Tl[3].Pt2->Pos[1],Tl[3].Pt2->Pos[2]);  
    printf("%g %g %g\n",Tl[3].Pt3->Pos[0],Tl[3].Pt3->Pos[1],Tl[3].Pt3->Pos[2]);     
    printf("\n");
    printf("\n");

    printf("%g %g %g \n",Tl[0].T1->Pt1->Pos[0],Tl[0].T1->Pt1->Pos[1],Tl[0].T1->Pt1->Pos[2]);  
    printf("%g %g %g \n",Tl[0].T1->Pt2->Pos[0],Tl[0].T1->Pt2->Pos[1],Tl[0].T1->Pt2->Pos[2]);
    printf("%g %g %g \n",Tl[0].T1->Pt3->Pos[0],Tl[0].T1->Pt3->Pos[1],Tl[0].T1->Pt3->Pos[2]);

  
    printf("sizeof TriangleInList = %d\n",sizeof(struct TriangleInList));
    
    
    printf("--------------------------------------------------------------\n");
    
    Te =   Tl[2].T1;		/* Tl[0]    --> -1 -1 -1 */
    
    
    printf("%g %g %g \n",Te->T1->Pt1->Pos[0],Te->T1->Pt1->Pos[1],Te->T1->Pt1->Pos[2]);
    printf("%g %g %g \n",Tl[0].T1->Pt1->Pos[0],Tl[0].T1->Pt1->Pos[1],Tl[0].T1->Pt1->Pos[2]);
     
     
    Te->T1 = &T2;
    printf("%g %g %g \n",Te->T1->Pt1->Pos[0],Te->T1->Pt1->Pos[1],Te->T1->Pt1->Pos[2]);
    printf("%g %g %g \n",Tl[0].T1->Pt1->Pos[0],Tl[0].T1->Pt1->Pos[1],Tl[0].T1->Pt1->Pos[2]);
    
    
    printf("%g %g %g \n",Tl[3].T1->Pt1->Pos[0],Tl[3].T1->Pt1->Pos[1],Tl[3].T1->Pt1->Pos[2]);
    
    
    
    
    
    
  
  }
